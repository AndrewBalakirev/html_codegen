# html_codegen

- `git clone https://gitlab.com/html_codegen/html_codegen.git`
- `cd html_codegen`
- `python3 -m venv venv && . venv/bin/activate`
- `pip install poetry`
- `poetry install`
- `python main.py`
